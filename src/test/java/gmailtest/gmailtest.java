package gmailtest;

import com.codeborne.selenide.Configuration;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static gmailtest.Config.login;
import static gmailtest.Config.password;

/**
 * Created by student on 25.05.15.
 */
public class gmailtest {
//comment
    @Test
     public void gmailTest(){
        Configuration.timeout = 32000;

        open("https://www.gmail.com/");
        $("#Email").setValue(login).pressEnter();
        $("#Passwd").setValue(password).pressEnter();
        $(byText("НАПИСАТЬ")).click();

        $(".aYF").shouldHave(text("новое сообщение"));
        $(By.name("to")).setValue(login);
        $(".aoT").setValue("Topic");
        $(byText("Отправить")).click();
        $(".TN.GLujEb").click();

        //$(".y6").shouldHave(text("Topic"));
        $(".zA.zE").shouldHave(text("Topic"));



    }




}
